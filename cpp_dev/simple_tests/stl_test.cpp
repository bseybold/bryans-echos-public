// copywrite Bryan Seybold, 2014/4/12
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <vector>
#include <iostream>


int main(){
	//create an int vector
	std::vector<int> v;

	//add ten numbers to it
	for(int i = 0; i < 10; i++){
	  v.push_back(i);
	}

	//create an int vector type to avoid typing namespace
	typedef std::vector<int> int_vec_t;
	//could use previous typedef as base for this one
	typedef std::vector<int>::iterator int_vec_itr_t;

	//print the contents using iterators
	std::cout << "vector contains:";
	for ( int_vec_t::const_iterator itr = v.begin(); itr != v.end(); itr++){
	  std::cout << " " << *itr;
	}
	std::cout << std::endl;

	return 0;
};

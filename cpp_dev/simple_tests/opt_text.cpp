// author: Bryan Seybold
// date: 2014/4/12
// copywrited
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <gsl/gsl_multimin.h>

//example from gsl optimization documentation

//function for minimization
//  x - vector of current position
//  params - data structure of parameters
//  f - pointer to output f(x,params)
//  g - pointer to output grad(f(x,params))

//base function
double my_f (const gsl_vector * v, void * params){
	double x,y;
	double *p = (double *)params;

	x = gsl_vector_get(v,0);
	y = gsl_vector_get(v,1);

	return ( p[2]*(x-p[0])*(x-p[0])+p[3]*(y-p[1])*(y-p[1]) + p[4] );
}
//gradient
void my_df (const gsl_vector * v, void * params, gsl_vector * df){
	double x,y;
	double *p = (double*)params;

	x = gsl_vector_get(v,0);
	y = gsl_vector_get(v,1);

	gsl_vector_set(df,0,2.0*p[2]*(x-p[0]));
	gsl_vector_set(df,1,2.0*p[3]*(y-p[1]));
}
//both at the same time
void my_fdf (const gsl_vector * x, void * params, 
		double * f, gsl_vector * df){
	*f = my_f(x,params);
	my_df(x,params,df);
}


//main function to run the gsl code
int main (void) {
	//declare book keeping variables
	size_t iter = 0;
	int status;

	//declare the minimizer properties
	const gsl_multimin_fdfminimizer_type *T;
	gsl_multimin_fdfminimizer *s;

	gsl_vector * x;
	gsl_multimin_function_fdf my_func;

	//actually declare the function we're optimizing
	//parabolied centered at (1,2) scale (10,20), min=30
	double p[5] = {1.0,2.0,10.0,20.0,30.0};
	my_func.n = 2;
	my_func.f = &my_f;
	my_func.df = &my_df;
	my_func.fdf = &my_fdf;
	my_func.params = (void *)p;

	x = gsl_vector_alloc(2);
	gsl_vector_set(x,0,5.0);
	gsl_vector_set(x,1,7.0);

	//editing this line choses the minimization algorithm
	T = gsl_multimin_fdfminimizer_vector_bfgs2;
	s = gsl_multimin_fdfminimizer_alloc(T,2);
	//initialize the minimizer
	gsl_multimin_fdfminimizer_set(s, &my_func, x, 0.01, 1e-4);

	do{
		// do next iteration
		iter++;
		status = gsl_multimin_fdfminimizer_iterate(s);
		if (status)
			break;
		//test if we're finished yet
		status = gsl_multimin_test_gradient(s->gradient, 1e-3);
		if (status == GSL_SUCCESS)
			printf ("Minimum found at:\n");
		//regardless of whether or not we finish, output state
		printf ("%5d %.5f %.5f %10.5f\n", iter,
				gsl_vector_get(s->x,0),
				gsl_vector_get(s->x,1),
				s->f);
	}while (status == GSL_CONTINUE && iter < 100);
	//always remember to free your variables
	gsl_multimin_fdfminimizer_free (s);
	gsl_vector_free (x);

	return 0;
}

// copywrite Bryan Seybold, 2014/4/12
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

//define some math
int main (void){
  double a[] = { 0.11, 0.12, 0.13,
	         0.21, 0.22, 0.23 };
  double b[] = { 1011, 1012,
	  	 1021, 1022,
		 1031, 1032 };
  double c[] = { 0.00, 0.00,
	  	 0.00, 0.00 };

  gsl_matrix_view A = gsl_matrix_view_array(a,2,3);
  gsl_matrix_view B = gsl_matrix_view_array(b,3,2);
  gsl_matrix_view C = gsl_matrix_view_array(c,2,2);

  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
		 1.0, &A.matrix, &B.matrix,
		 0.0, &C.matrix);

  printf("[ %g, %g\n  %g, %g ]\n", c[0], c[1], c[2], c[3]);

  return 0;
}

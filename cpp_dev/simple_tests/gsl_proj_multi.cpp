// copywrite Bryan Seybold, 2014/4/12
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

const int kN_OBJS = 4;
const int kN_DIM  = 2;

int main (void){
  //a kN_DIM x 1 vector to project onto
  double a[] = { 1.0, -1.0 };
  //a kN_OBJS x kN_DIM matrix
  double b[] = { 1, 1,
	  	 1, 0,
		 1, -1,
  		 0, 1};
  //a kN_OBJS x 1 vector of projections
  double c[] = { 0.00, 0.00, 0.00, 0.00 };

  //create views for these arrays as matricies
  gsl_matrix_view A = gsl_matrix_view_array(a,kN_DIM,1);
  gsl_matrix_view B = gsl_matrix_view_array(b,kN_OBJS,kN_DIM);
  gsl_matrix_view C = gsl_matrix_view_array(c,kN_OBJS,1);

  //call the wrapped cblas function
  // dgemm   (double, general, matrix multiply)
  // first two variables could tanspose A & B efficiently
  // third scales the product of matrix multiply
  // fourth and fifth are B and A (flipped from definition for alignment)
  // fifth scales the output vector before the product is added into it
  // last is the output vector where things are written to
  gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
		 1.0, &B.matrix, &A.matrix,
		 0.0, &C.matrix);

  printf("output:\n");
  for (int i=0; i < kN_OBJS; i++){
	  printf(" %g \n", c[i]);
  }

  return 0;
}
